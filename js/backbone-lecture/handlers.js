define(
    'handlers',
    ['jquery', 'democode'],
    function($, democode){
        return {
            'apply': function(){

                $('#demo-code-step-5').on('click', function(){
                    console.log('democode step 5');
                    democode.step_5_demoRun();
                });

                $('#demo-code-step-6-1').on('click', function(){
                    democode.step_6_1_demoRun();
                });

                $('#demo-code-step-6-2').on('click', function(){
                    democode.step_6_2_demoRun();
                });

                $('#demo-code-step-8').on('click', function(){
                    democode.step_8_demoRun();
                });
                $('#demo-code-step-9-1').on('click', function(){
                    democode.step_9_1_demoRun();
                });
                $('#demo-code-step-9-2').on('click', function(){
                    democode.step_9_2_demoRun();
                });
                $('#demo-code-step-10-1').on('click', function(){
                    democode.step_10_1_demoRun();
                });
                $('#demo-code-step-10-2').on('click', function(){
                    democode.step_10_2_demoRun();
                });
                $('#demo-code-step-11-1').on('click', function(){
                    democode.step_11_1_demoRun();
                });
                $('#demo-code-step-11-2').on('click', function(){
                    democode.step_11_2_demoRun();
                });
                $('#demo-code-step-12-1').on('click', function(){
                    democode.step_12_1_demoRun();
                });
                $('#demo-code-step-12-2').on('click', function(){
                    democode.step_12_2_demoRun();
                });
                $('#demo-code-step-13-1').on('click', function(){
                    democode.step_13_1_demoRun();
                });
                $('#demo-code-step-13-2').on('click', function(){
                    democode.step_13_2_demoRun();
                });
                $('#demo-code-step-14').on('click', function(){
                    democode.step_14_demoRun();
                });
                $('#demo-code-step-15').on('click', function(){
                    democode.step_15_demoRun();
                });
                $('#demo-code-step-16').on('click', function(){
                    democode.step_16_demoRun();
                });
                $('#demo-code-step-17').on('click', function(){
                    democode.step_17_demoRun();
                });
                $('#demo-code-step-19').on('click', function(){
                    democode.step_19_demoRun();
                });
                $('#demo-code-step-20').on('click', function(){
                    democode.step_20_demoRun();
                });
                $('#demo-code-step-21').on('click', function(){
                    democode.step_21_demoRun();
                });
                $('#demo-code-step-22').on('click', function(){
                    democode.step_22_demoRun();
                });
                $('#demo-code-step-23').on('click', function(){
                    democode.step_23_demoRun();
                });
                $('#demo-code-step-25').on('click', function(){
                    democode.step_25_demoRun();
                });
                $('#demo-code-step-26').on('click', function(){
                    democode.step_26_demoRun();
                });
                $('#demo-code-step-27').on('click', function(){
                    democode.step_27_demoRun();
                });

                $('#demo-code').on('click', function(){
                    $(this).closest('#slide-body').children('.code-sample').toggle();
                    $(this).closest('#slide-body').children('#slide-content').toggle()
                });


            }
        }
});

