/**
 * Created by abdrus on 26.05.14.
 */
require.config({
    baseUrl: '../js/lib',
    paths: {
        'backbone':         'backbone/backbone',
        'impress' :         'impress/impress',
        'jquery':           'jquery/jquery.min',
        'underscore' :      'underscore/underscore',
        'rainbow':          'rainbow/rainbow',
        'rainbow_language': 'rainbow/language/javascript',
        'rainbow_generic':  'rainbow/language/generic',
        'text':             'require/text/text',
        'handlers':         '../backbone-lecture/handlers',
        'democode':         '../backbone-lecture/democode'
    },
    shim: {
        'backbone': {
            deps: ['jquery', 'underscore'],
            exports: 'backbone'
        },
        'impress': {
            exports: 'impress'
        },
        'jquery': {
            exports: '$'
        },
        'underscore': {
            exports: '_'
        },
        'rainbow': {
            deps: ['backbone', 'jquery'],
            exports: 'rainbow'
        },
        'rainbow_generic': {
            deps: ['rainbow'],
            exports: 'rainbow_generic'
        },
        'rainbow_language': {
            deps: ['rainbow', 'rainbow_generic'],
            exports: 'rainbow_language'
        }

    }
});

var presentationConfig = {
    slides : [
        'step-1.html',
        'step-2.html',
        'step-3.html',
        'step-4.html',
        'step-5.html',
        'step-6.html',
        'step-7.html',
        'step-8.html',
        'step-9.html',
        'step-10.html',
        'step-11.html',
        'step-12.html',
        'step-13.html',
        'step-14.html',
        'step-15.html',
        'step-16.html',
        'step-17.html',
        'step-18.html',
        'step-19.html',
        'step-20.html',
        'step-21.html',
        'step-22.html',
        'step-23.html',
        'step-24.html',
        'step-25.html',
        'step-26.html',
        'step-27.html',
        'step-28.html',
        'step-29.html',
        'step-30.html'
    ]
}

require(
    [
        'backbone',
        'impress',
        'jquery',
        'underscore',
        'rainbow',
        'rainbow_generic',
        'rainbow_language',
        'text',
        'handlers',
        'democode'
    ],
    function(backbone, impress, $, _, rainbow, rainbow_generic, rainbow_language, text, handlers, democode)
    {
        _.each(presentationConfig.slides, function(filename, index){
            require(['text!../../backbone-js/content/' + filename], function(html){
                $('#step-' + (index+1)).html(html);
                rainbow.color();
                handlers.apply();
            });
        });

        var impress = impress();
        impress.init();

        var rainbow = Rainbow;
        rainbow.color();
    }
);
