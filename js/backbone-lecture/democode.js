define(
    'democode',
    ['jquery', 'backbone', 'underscore'],
    function($, Backbone, _){

        console.log('democode enviroment initialized');

        var TodoItem = Backbone.Model.extend({
            urlRoot: '/todos',
            viewCommunication: function(){
                console.log('Ссылка на модель сработала, вызван ее метод');
            }
        });
        var todoItem = new TodoItem({ description: 'Pick up milk', status: 'incomplete', id: 1 });

        var TodoView = Backbone.View.extend({
            render: function(){
                var html = '<h3>' + this.model.get('description') + '</h3>';
                $(this.el).html(html);
            }
        });
        var todoView = new TodoView({ model: todoItem });

        // Slide 12
        var TodoItemDefVal = Backbone.Model.extend({
            defaults: {
                description: 'Default value',
                status: 'incomplete'
            }
        });

        //Slide 14
        var TodoViewAddParams = Backbone.View.extend({
            tagName: 'article',
            id: 'todo-view',
            className: 'todo'
        });

        //Slide 15-16-17
        var TodoViewTemplated = Backbone.View.extend({
            template: _.template('<h3><%= description %></h3>'),
            events: {
                'click h3': 'alertStatus'
            },
            render: function(){
                var attributes = this.model.toJSON();
                this.$el.html(this.template(attributes));
            },
            renderSlide16: function(){
                var attributes = this.model.toJSON();
                this.$el.html(this.template(attributes));
                $('#slide-16-h3-place').html(this.el);
            },
            renderSlide17: function(){
                var attributes = this.model.toJSON();
                this.$el.html(this.template(attributes));
                $('#slide-17-h3-place').html(this.el);
            },
            alertStatus: function(e){
                alert('Hey you clicked the h3!');
            },
            modelCommunication: function(){
                this.model.viewCommunication();
            }
        });

        //Slide 19
        var TodoList = Backbone.Collection.extend({model: TodoItem});
        var todoList = new TodoList;

        //slide 23
        var TodoListView = Backbone.View.extend({
            'render': function(){
                this.collection.forEach(this.addOne, this);
            },
            'addOne': function(todoItem){
                var todoView = new TodoView({'model': todoItem});
                todoView.render();
                this.$el.append(todoView.el);
            }
        });

        //slide 26
        var Router = Backbone.Router.extend({
            routes: {
                'todos': 'showList',
                'todos/:param': 'paramRouteHandler'
            },
            showList: function(){
                console.log('Пользователь перешел по URL /todos');
            },
            paramRouteHandler: function(param){
                console.log('Сработал обработчик параметризованного маршрута. Параметр: ' + param);
            },
            start: function(){
                Backbone.history.start();
            }
        });

        return{
            'step_5_demoRun': function(){
                console.clear();

                console.log('Класс-наследник модели (TodoItem): ')
                console.dir(TodoItem);
                console.log('Экземпляр класса (todoItem): ')
                console.log(todoItem);
            },
            'step_6_1_demoRun': function(){
                console.clear();

                console.log('Доступ к полю объекта:');
                console.log(todoItem.get('description'));
            },
            'step_6_2_demoRun': function(){
                console.log('Установка атрибута объекта: ');
                todoItem.set({status: 'complete'});
                console.dir(todoItem);
            },
            'step_8_demoRun': function(){
                console.clear();

                console.log('Наследник вида (TodoView): ');
                console.dir(TodoView);
            },
            'step_9_1_demoRun': function(){
                console.clear();
                console.log('Экземпляр вида (todoView): ');
                console.dir(todoView);
            },
            'step_9_2_demoRun': function(){
                todoView.render();
                console.log('Рендер модели видом (todoView): ');
                console.dir(todoView.el);
            },
            'step_11_1_demoRun': function(){
                console.clear();

                var todoItemToFetch = new TodoItem({id: 1});
                todoItemToFetch.fetch();
                console.log('Модель, полученная с бакенда');
                console.dir(todoItemToFetch);
            },
            'step_11_2_demoRun': function(){
                var todoItemToFetch = new TodoItem({id: 1});
                todoItemToFetch.fetch();

                todoItemToFetch.set({'description': 'Pick up cookies.'});
                console.log('Изменяем модель');
                todoItemToFetch.save();
            },
            'step_12_1_demoRun': function(){
                console.clear();
                console.log('Экземпляр класса, устанавливающий значения по умолчанию: ');
                console.dir(TodoItemDefVal);
            },
            'step_12_2_demoRun': function(){
                var todoItemDefVal = new TodoItemDefVal();
                console.log('todoItemDefVal: ');
                console.dir(todoItemDefVal);
            },
            'step_13_1_demoRun': function(){
                console.clear();
                todoItem.on('change:description', function(){
                    console.log('Внимание! Изменилось свойство description!');
                });
                console.log('Изменяем поле description');
                todoItem.set({'description': 'Поиграть в боулинг'});
                console.dir(todoItem);
            },
            'step_13_2_demoRun': function(){
                todoItem.trigger('change:description');
            },
            'step_14_demoRun': function(){
                console.clear();

                var customizedView = new TodoViewAddParams();
                console.log('customizedView: ');
                console.dir(customizedView);
            },
            'step_15_demoRun': function(){
                console.clear();

                var view = new TodoViewTemplated({model: todoItem});
                view.render();
                console.dir(view.el);
            },
            'step_16_demoRun': function(){
                console.clear();

                var view = new TodoViewTemplated({model: todoItem});
                view.renderSlide16();
            },
            'step_17_demoRun': function(){
                console.clear();

                var view = new TodoViewTemplated({
                    model: todoItem,
                    events: {
                        'click': 'modelCommunication'
                    }
                });
                view.renderSlide17();
            },
            'step_19_demoRun': function(){
                console.clear();

                console.log('Объект коллекции: ');
                console.dir(todoList);

                console.log('Добавление в коллекцию');
                todoList.add(todoItem);

                console.log('Количество моделей в коллекции');
                console.log(todoList.length);

                console.log('Получение модели с index 0');
                console.dir(todoList.at(0));

                console.log('Получение модели с id=1');
                console.dir(todoList.get(1));

                console.log('Удаление из коллекции');
                todoList.remove(todoItem);
                console.log(todoList.length);
            },
            'step_20_demoRun': function(){
                console.clear();

                var todos = [
                    {'description': 'Выпрямить глушитель', 'status': 'incomplete'},
                    {'description': 'Поменять ступичные подшипники', 'status': 'incomplete'},
                    {'description': 'Устранить течь из КПП', 'status': 'incomplete'}
                ]
                todoList.reset(todos);

                console.dir(todoList.models);
            },
            'step_21_demoRun': function(){
                console.clear();

                var todos = [
                    {'description': 'Выпрямить глушитель', 'status': 'incomplete'},
                    {'description': 'Поменять ступичные подшипники', 'status': 'incomplete'},
                    {'description': 'Устранить течь из КПП', 'status': 'incomplete'}
                ]
                todoList.reset(todos);

                todoList.on('change', function(todoItem){
                    console.log('Изменения в коллекции:');
                    console.log(todoItem.get('description'));
                    console.log(todoItem.get('status'));
                });

                var obj = todoList.at(0);
                obj.set({'description': 'Купить расходники', 'status': 'complete'});
            },
            'step_22_demoRun': function(){
                var todos = [
                    {'description': 'Купить расходники', 'status': 'complete'},
                    {'description': 'Поменять ступичные подшипники', 'status': 'incomplete'},
                    {'description': 'Устранить течь из КПП', 'status': 'incomplete'}
                ]
                todoList.reset(todos);

                todoList.forEach(function(todoItem){
                    console.dir(todoItem.attributes);
                });
            },
            'step_23_demoRun': function(){
                console.clear();

                var todos = [
                    {'description': 'Купить расходники', 'status': 'complete'},
                    {'description': 'Поменять ступичные подшипники', 'status': 'incomplete'},
                    {'description': 'Устранить течь из КПП', 'status': 'incomplete'}
                ]

                todoList.reset(todos);

                var todoListView = new TodoListView({'collection': todoList});
                todoListView.render();
                console.dir(todoListView.el);
            },
            'step_26_demoRun': function(){
                $(function() {
                    var router = new Router();
                    console.log('Роутер создан');
                    router.start();
                    console.dir(router);
                });
            },
            'step_27_demoRun': function(){
                $(function() {

                });
            }
        }
});