/**
 * Created by abdrus on 26.05.14.
 */
require.config({
    baseUrl: '../js/lib',
    paths: {
        'underscore' :      'underscore/underscore',
        'impress' :         'impress/impress',
        'jquery':           'jquery/jquery.min',
        'highlightpack':    'highlight/highlight.pack',
        'text':             'require/text/text',
        'lightbox':         'lightbox/lightbox.min'
    },
    shim: {
        'lightbox': {
            deps: ['jquery'],
            exports: 'lightbox'
        },
        'impress': {
            exports: 'impress'
        },
        'jquery': {
            exports: '$'
        },
        'highlight.pack': {
            deps: ['text'],
            exports: 'highlightpack'
        }

    }
});

var presentationConfig = {
    slides : [
        'step-1.html',
        'step-2.html',
        'step-3.html',
        'step-4.html',
        'step-5.html',
        'step-6.html',
        'step-7.html',
        'step-8.html',
        'step-9.html',
        'step-10.html',
        'step-11.html',
        'step-12.html',
        'step-13.html',
        'step-14.html',
        'step-15.html',
        'step-16.html',
        'step-17.html',
        'step-18.html',
        'step-19.html',
        'step-20.html',
        'step-21.html',
        'step-22.html',
        'step-23.html',

        'step-24.html',
        'step-25.html',
        'step-26.html',
        'step-27.html',
        'step-28.html',
        'step-29.html',
    ]
}

require(
    [
        'lightbox',
        'underscore',
        'impress',
        'jquery',
        'highlightpack',
        'text',
    ],
    function(lightbox, _, impress, $, rainbow, highlightpack, text)
    {
        var impress = impress();
        impress.init();

        var highlightpack = hljs;
        highlightpack.initHighlightingOnLoad();

        _.each(presentationConfig.slides, function(filename, index){
            require(['text!../../script-languages-ospu-javascript/content/' + filename], function(html){
                $('#step-' + (index+1)).html(html);
                hljs.initHighlighting();
            });
        });

    }
);
