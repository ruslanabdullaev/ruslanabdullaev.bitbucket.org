/**
 * Created by abdrus on 26.05.14.
 */
require.config({
    baseUrl: '../js/lib',
    paths: {
        'underscore' :      'underscore/underscore',
        'impress' :         'impress/impress',
        'jquery':           'jquery/jquery.min',
        'highlightpack':    'highlight/highlight.pack',
        'text':             'require/text/text',
        'lightbox':         'lightbox/lightbox.min',
        'handlers':         '../script-languages-ospu-python/handlers',
        'democode':         '../script-languages-ospu-python/democode'
    },
    shim: {
        'lightbox': {
            deps: ['jquery'],
            exports: 'lightbox'
        },
        'impress': {
            exports: 'impress'
        },
        'jquery': {
            exports: '$'
        },
        'highlight.pack': {
            deps: ['text'],
            exports: 'highlightpack'
        }

    }
});

var presentationConfig = {
    slides : [
        'step-1.html',
        'step-2.html',
        'step-3.html',
        'step-4.html',
        'step-5.html',
        'step-6.html',
        'step-7.html',
        'step-8.html',
        'step-9.html',
        'step-10.html',
        'step-11.html',
        'step-12.html',
        'step-13.html',
        'step-14.html',
        'step-15.html',
        'step-16.html',
        'step-17.html',
        'step-18.html',
        'step-19.html',
        'step-20.html',
        'step-21.html',
        'step-22.html',
        'step-23.html',
        'step-24.html',
        'step-25.html',
        'step-26.html',
        'step-27.html',
        'step-28.html',
        'step-29.html',
        'step-30.html',
        'step-31.html',
        'step-32.html',
        'step-33.html',
        'step-34.html',
        'step-35.html',
        'step-36.html',
        'step-37.html',
        'step-38.html',
        'step-39.html',
        'step-40.html',
        'step-41.html',
        'step-42.html',
        'step-43.html',
        'step-44.html',
        'step-45.html',
        'step-46.html',
        'step-47.html',
        'step-48.html',
        'step-49.html',
        'step-50.html',
        'step-51.html',
        'step-52.html',
        'step-53.html',
        'step-54.html',
        'step-55.html',
        'step-56.html',
        'step-57.html',
        'step-58.html',
        'step-59.html',
        'step-60.html',
        'step-61.html',
        'step-62.html',
        'step-63.html',
        'step-64.html',
        'step-65.html',
        'step-66.html',
        'step-67.html',
        'step-68.html',
        'step-69.html',
        'step-70.html',
        'step-71.html',
        'step-72.html',
        'step-73.html',
        'step-74.html',
        'step-75.html',
        'step-76.html',
        'step-77.html',
        'step-78.html',
        'step-79.html',
        'step-80.html',
        'step-81.html',
        'step-82.html',
        'step-83.html',
        'step-84.html',
        'step-85.html',
        'step-86.html',
        'step-87.html',
        'step-88.html',
        'step-89.html',
        'step-90.html',
        'step-91.html',
        'step-92.html',
        'step-93.html',
        'step-94.html',
        'step-95.html',
        'step-96.html',
        'step-97.html',
        'step-98.html',
        'step-99.html',
        'step-100.html',
        'step-101.html',
        'step-102.html',
    ]
}

require(
    [
        'lightbox',
        'underscore',
        'impress',
        'jquery',
        'highlightpack',
        'text',
        'handlers',
        'democode'
    ],
    function(lightbox, _, impress, $, rainbow, highlightpack, text, handlers, democode)
    {
        var impress = impress();
        impress.init();

        var highlightpack = hljs;
        highlightpack.initHighlightingOnLoad();

        _.each(presentationConfig.slides, function(filename, index){
            require(['text!../../script-languages-ospu-python/content/' + filename], function(html){
                $('#step-' + (index+1)).html(html);
                hljs.initHighlighting();
            });
        });

    }
);
